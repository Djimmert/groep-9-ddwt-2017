<?php
  session_start();
  require_once "../config.inc.php";
  $updateNameData = new PDO("mysql:dbname={$config['db_name']};host={$config['db_host']}",$config['db_user'], $config['db_pass']);
  $updateFirstName = $updateNameData->prepare('UPDATE user_info SET First_Name = ? WHERE username = ?');
  $updateFirstName->execute(array($_POST['firstname'], $_SESSION['user_session']));
  $updatelastName = $updateNameData->prepare('UPDATE user_info SET Last_Name = ? WHERE username = ?');
  $updatelastName->execute(array($_POST['lastname'], $_SESSION['user_session']));
  header('Location: ./index.php');
;
?>
