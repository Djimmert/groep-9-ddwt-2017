<?php
session_start();
require_once "../config.inc.php";
require_once "../password.inc.php";
$updateImage = new PDO("mysql:dbname={$config['db_name']};host={$config['db_host']}",$config['db_user'], $config['db_pass']);
mkdir("uploads/{$_SESSION['user_session']}/", 0755, true);
chmod("uploads/{$_SESSION['user_session']}/", 0755);
$target_dir = "uploads/{$_SESSION['user_session']}/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        $uploadOk = 1;
    } else {
        $error1 = "File is not an image.";
        echo "<script type='text/javascript'>alert('$error1');</script>";
        $uploadOk = 0;
    }
}

// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    $error3 = "Sorry, your file is too large.";
    echo "<script type='text/javascript'>alert('$error3');</script>";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    $error4 = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    echo "<script type='text/javascript'>alert('$error4');</script>";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    $error5 = "Sorry, your file was not uploaded.";
    echo "<script type='text/javascript'>alert('$error5');</script>";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        chmod($target_file, 0755);
        $succes = "Profile picture succesfully updated.";
        echo "<script type='text/javascript'>alert('$succes');</script>";
    } else {
        $error6 = "Sorry, there was an error uploading your file.";
        echo "<script type='text/javascript'>alert('$error6');</script>";
    }
}
$image = $updateImage->prepare('UPDATE user_info SET image = ? WHERE username = ?');
$image->execute(array($target_file, $_SESSION['user_session']));
header('Refresh:0; ./index.php');
?>


