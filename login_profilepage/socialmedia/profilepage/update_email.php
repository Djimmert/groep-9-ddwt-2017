<?php
  session_start();
  require_once "../config.inc.php";
  $updateEmailData = new PDO("mysql:dbname={$config['db_name']};host={$config['db_host']}",$config['db_user'], $config['db_pass']);
  $updateEmail = $updateEmailData->prepare('UPDATE user_info SET Email = ? WHERE username = ?');
  $updateEmail->execute(array($_POST['email'], $_SESSION['user_session']));
  header('Location: ./index.php');
;
?>
