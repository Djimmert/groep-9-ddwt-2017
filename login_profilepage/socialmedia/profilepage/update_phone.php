<?php
  session_start();
  require_once "../config.inc.php";
  $updatePhoneData = new PDO("mysql:dbname={$config['db_name']};host={$config['db_host']}",$config['db_user'], $config['db_pass']);
  $updatePhone = $updatePhoneData->prepare('UPDATE user_info SET Phone_Number = ? WHERE username = ?');
  $updatePhone->execute(array($_POST['phone'], $_SESSION['user_session']));
  header('Location: ./index.php');
;
?>
