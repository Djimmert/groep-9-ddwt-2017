<?php
require_once('config_stefan.inc.php');
require_once('header.php');

$qh = $dbh->prepare(
    'SELECT * FROM users_friends WHERE user_id = ?'
);
$qh->execute(array($_SESSION['user_id']));


?>


        <div class="row marketing">
          <div class="col-lg-6">



                  <h3>Search Friends</h3>
              <p>Search on username:</p>
              <form  method="post" action="search.php"  id="searchform">
                  <input  type="text" name="searchn">
                  <input  type="submit" name="submit" value="Search">
              </form>


          </div>

          <div class="col-lg-6">
            <h4>Current Friends</h4>


              <table class="table">
                  <?php

                  foreach ($qh as $row) {
                      $qh2 = $dbh->prepare(
                          'SELECT * FROM Users WHERE user_id = ?'
                      );
                      $qh2->execute(array($row['friend_user_id']));

                      $result = $qh2->fetch(PDO::FETCH_ASSOC);

                      ?>
                      <tr>
                          <td><?php echo htmlspecialchars($result['name'], ENT_QUOTES) ?></td>
                      </tr>
                      <?php

                  }

                  ?>
              </table>


          </div>
        </div>

<?php
require_once('footer.php');
?>
