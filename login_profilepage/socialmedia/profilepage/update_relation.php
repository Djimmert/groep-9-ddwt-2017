<?php
  session_start();
  require_once "../config.inc.php";
  $updateRelationData = new PDO("mysql:dbname={$config['db_name']};host={$config['db_host']}",$config['db_user'], $config['db_pass']);
  $updateRelation = $updateRelationData->prepare('UPDATE user_info SET Relationship_Status = ? WHERE username = ?');
  $updateRelation->execute(array($_POST['relation'], $_SESSION['user_session']));
  header('Location: ./index.php');
;
?>
