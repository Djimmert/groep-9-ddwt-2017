<?php
error_reporting(-1);
ini_set("display_errors", 1);
session_start();
require 'create.php';
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset='UTF-8'>
  <title>The Home Network</title>
  <link rel=stylesheet href=../../../Site/Stylesheets/main.css>
</head>
<body>
<header><a href=../../../><img src=../../../Site/Media/greenhome.ico></a></header>
  <main id=reg>
    <form id=createForm action=create.php method=POST>
      <h2> Register </h2>
      <p>Username: </p><input type=text id=username name=username placeholder=Username><br>
      <p>Password: </p><input type=password id=password name=password placeholder=Password><br>
      <p>Verify password: </p><input type=password id=Verify name=Verify placeholder=Verify password><br>
      <p>First Name: </p><input type=text id=Firstname name=Firstname placeholder=John><br>
      <p>Last Name: </p><input type=text id=Lastname name=Lastname placeholder=Doe><br>
      <p>E-mail: </p><input type=text id=email name=email placeholder=JohnDoe@hotmail.com><br>
      <p>Phonenumber: </p><input type=text id=phone name=phone placeholder=0612345678><br>
      <p>Gender: </p><select name=sex><option value='Male'>Male</option><option value='Female'>Female</option><option value='Other'>Other</option></select><br>
      <p>Relationship: </p><select name=RelationshipStatus><option value='Single'>Single</option><option value='Married'>Married</option><option value='In a relationship'>In a relationship</option></select><br>
      <input class=create type=submit value='Create account'>
    </form>
  </main>
  <footer>Logo of sitenaam of whatever</footer>
</body>
</html>
