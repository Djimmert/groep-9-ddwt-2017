<?php
  require_once "../config.inc.php";
  require_once "../password.inc.php";
  $dbh = new PDO("mysql:dbname={$config['db_name']};host={$config['db_host']}",$config['db_user'], $config['db_pass']);
  $checkUser = $dbh->prepare('SELECT * FROM Users WHERE name = ?');
  $user = isset($_POST['username']) ? $_POST['username'] : '';
  $user = !empty($_POST['username']) ? $_POST['username'] : '';
  $checkUser->execute(array($user));
  $row = $checkUser->fetch();
  $succes = "Account has been created.";
  $passwordError = "Passwords do not match.";
  $usernameError = "Username is already in use.";
  $placeholderImage = "uploads/placeholder.jpeg";
  if ($row) {
    $dataCheck = false;
    echo "<script type='text/javascript'>alert('$usernameError');</script>";
    header('Refresh:0; ./create_form.php');
  } else {
      if (isset($_POST['username']) && isset($_POST['password'])) {
        if ($_POST['password'] === $_POST['Verify']) {
          $hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
          $query = $dbh->prepare("INSERT INTO Users (name, password) VALUES (?, ?)");
          $query->execute(array($_POST['username'], $hash)); 
          $user_info = $dbh->prepare("INSERT INTO user_info (First_Name, Last_Name, Email, Phone_Number, Sex, image, Relationship_Status, username) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
          $user_info->execute(array($_POST['Firstname'],$_POST['Lastname'],$_POST['email'],$_POST['phone'],$_POST['sex'], $placeholderImage,$_POST['RelationshipStatus'], $_POST['username']));
          echo "<script type='text/javascript'>alert('$succes');</script>";
          header('Refresh:0; ../../../index.php');
        } else {
            echo "<script type='text/javascript'>alert('$passwordError');</script>";
            header('Refresh:0; ./create_form.php');
      }
    }
  }    
?>
