<?php
error_reporting(-1);
ini_set("display_errors", 1);
session_start();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset='UTF-8'>
  <title>The Home Network</title>
  <link rel=stylesheet href=../../../Site/Stylesheets/main.css>
</head>
<body>
<header><a href=../../../><img src=../../../Site/Media/greenhome.ico></a></header>
  <main id=reg>
    <form id=loginForm action=login.php method=POST>
      <h2>Login</h2>
      <input type=text id=loginUsername name=loginUsername placeholder=Username><br>
      <input type=password id=loginPassword name=loginPassword placeholder=Password><br>
      <input class=login type=submit value='Log in'>
    </form>
  </main>
