<?php
  session_start();
  require_once "../config.inc.php";
  require_once "../password.inc.php";
  $db = new PDO("mysql:dbname={$config['db_name']};host={$config['db_host']}",$config['db_user'], $config['db_pass']);
  if (isset($_POST['loginUsername']) && isset($_POST['loginPassword'])) {
    $select = $db->prepare('SELECT * FROM Users WHERE name = ?');
    $select->execute(array($_POST['loginUsername']));
    $row = $select->fetch();
    if ($row && password_verify($_POST['loginPassword'], $row['password_hash'])) {
        $_SESSION['user_id'] = $row['user_id'];
        $_SESSION['user_session'] = $row['name'];
      header('Location: ../profilepage');
    } else {
        $message = "Invalid username or password.";
        echo "<script type='text/javascript'>alert('$message');</script>";
        header('Refresh:0; ./index.php');
    }
}
?>
