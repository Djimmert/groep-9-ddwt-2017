CREATE TABLE Users ( 
        user_id int not null AUTO_INCREMENT,
	name varchar(255) not null,
	password_hash varchar(255) not null,	
	PRIMARY KEY (user_id),
        UNIQUE (`name`)
);

CREATE TABLE user_info (
        Firstname varchar(50) not null,
        Lastname varchar(50) not null,
        Email varchar(100) not null,
        Phone_Number varchar(10) not null,
        Sex varchar(10) not null,
        Relationship_Status varchar(50) not null,
        username varchar(255) not null,
        image varchar(255),
        PRIMARY KEY (username),
        UNIQUE (`username`)
);

CREATE TABLE `users_friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `friend_user_id` int(11) NOT NULL
);

ALTER TABLE `users_friends` ADD UNIQUE( `user_id`, `friend_user_id`);
