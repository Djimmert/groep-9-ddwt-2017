<?php
/**
 * Created by PhpStorm.
 * User: Djim
 * Date: 12/12/2017
 * Time: 11:19
 */
error_reporting(-1);
ini_set("display_errors", 1);
session_start();
?>
<!doctype html>
<html>
<head>
    <meta charset=utf-8>
    <link rel=stylesheet href=Stylesheets/main.css>
    <title>The Home Network</title>
</head>
<body>
    <header><a href=../><img src=Media/greenhome.ico></a></header>
    <main>
        <?php
        if (isset($_SESSION['user_id'])) {
            ?>
            <a class=button href=logout.php>Log out</a>
            <?php
        } else {
            ?>
            <a class=button href=../login_profilepage/socialmedia/users/login_form.php>Log in</a><a class=button href=../login_profilepage/socialmedia/users/create_form.php>Create account</a>
            <?php
        }
        ?>
    </main>


